var mongoose = require('mongoose');
var Client = mongoose.model('Client');
var configuration = require('../conf/configuration');
var crypto = require('crypto');

//all clients
exports.findAll = function(req, res){
    console.log('GET/clients');
    Client.find(function(err, clients) {
        if (err)
            return res.status(500).send({message: err.message});
        if (clients.length < 1)
            return res.status(404).send('No Users found!');
        return res.status(200).jsonp(clients);
    });
};
//client by email
exports.findByEmail = function(req, res){
    console.log('GET/clients/'+req.params.email);
    Client.find({email: req.params.email}, function(err, client){
        if (err)
            return res.status(500).send({message: err.message});
        if (client.length < 1)
            return res.status(404).send({message: 'User not found!'});
        return res.status(200).jsonp(client);
    });
};
//add a client
exports.add = function(req, res){
    console.log('POST/clients');
    var client = new Client({
        firstName: req.body.name?req.body.name:"Pancho",
        lastName: req.body.last?req.body.last:"Lopez",
        email: req.body.email,
        password: encrypt(req.body.password),
        genre: req.body.genre?req.body.genre:"female",
        location: {
            coordinates: [req.body.latitude?req.body.latitude:0,req.body.longitude?req.body.longitude:0]
        },
        address: {
            street: req.body.street?req.body.street:"",
            number: req.body.number?req.body.number:"",
            district: req.body.district?req.body.district:"",
            cp: req.body.cp?req.body.cp:"",
            reference: req.body.reference?req.body.reference:""
        }
    });
    console.log('datos:', client);
    client.save(function(err, client){
        if (err){
            if (err.message.includes("duplicate"))
                return res.status(403).send({message : 'This email already exists!'});
            return res.status(500).send({message: err.message});
        }
        return res.status(200).json(client);
    });
};
//update a client
exports.update = function(req, res) {
    Client.find({email: req.params.email}, function(err, client) {
        if (client.length < 1)
            return res.status(404).send({message: 'User Not Found!'});
        client.firstName= req.body.name?req.body.name:client.firstName;
        client.lastName= req.body.last?req.body.last:client.lastName;
        client.email= req.body.email?req.body.email:client.email;
        client.password= req.body.password?encrypt(req.body.password):client.password;
        client.genre= req.body.genre?req.body.genre:client.genre;
        client.location.coordinates[0] = req.body.latitude?req.body.latitude:client.location.coordinates[0];
        client.location.coordinates[1] = req.body.longitude?req.body.longitude:client.location.coordinates[1];
        client.address.stret = req.body.street?req.body.street:client.address.street;
        client.address.number = req.body.number?req.body.number:client.address.number;
        client.address.district = req.body.district?req.body.district:client.address.firstName;
        client.address.cp = req.body.cp?req.body.cp:client.address.cp;
        client.address.reference = req.body.reference?req.body.reference:client.address.reference;
        console.log('PUT/clients/'+req.params.email);
        console.log(client);
        client.save(function(err) {
            if(err)
                return res.status(500).send({message: err.message});
            return res.status(200).jsonp(client);
        });
    });
   };
   
//delete a client by email
exports.delete = function(req, res) {
    console.log('DELETE/clients/'+req.params.email);
    Client.find({email: req.params.email}, function(err, client) {
        if (client.length < 1)
            return res.status(404).send({message: 'User Not Found'});
        Client.deleteOne({email: req.params.email},function(err, result) {
            if(err)
                return res.status(500).send({message: err.message});
            if (result.deletedCount == 0)
                return res.status(500).send({message: 'User couldnt delete'});
            return res.json({ message: 'Successfully deleted'});
        });
        
    });
};
//function to encrypt
function encrypt(text){
    var  cipher = crypto.createCipher(configuration.ALGORITHM, configuration.SECRET);
    var crypted = cipher.update(text,'utf8','hex');
    crypted += cipher.final('hex');
    return crypted;
}
