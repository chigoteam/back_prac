var mongoose = require('mongoose');
var Movement = mongoose.model('Movement');
//all movements
exports.findAll = function(req, res){
    console.log('GET/movements');
    Movement.find(function(err, movements) {
        if (err)
            return res.status(500).send({message: err.message});
        return res.status(200).jsonp(movements);
    });
};
//movements by client
exports.findByMail = function(req, res){
    console.log('GET/movements/'+req.params.email);
    Movement
        .find({ email: req.params.email })
        .exec(function(err, client){
            if (err)
                return res.status(500).send({message: err.message});
            if (client.length < 1)
                return res.status(404).send({message : "This user does not have movements"})
            return res.status(200).jsonp(client);
        });
};
//add a movement
exports.add = function(req, res){
    console.log('POST/movements');
    var movement = new Movement({
        email: req.body.email,
        date: req.body.date,
        type: req.body.type,
        quantity: req.body.quantity,
        cost: req.body.cost,
        product: req.body.product
    });
    console.log('datos:', movement);
    movement.save(function(err){
        if (err)
            return res.status(500).send({message: err.message});
        return res.status(200).json(movement);
    });
};
