# Backend para aplicación OKTLI

## Autores
* Omar Chigo Acua
* Con colaboración de César Osvaldo Martínez Cantú

## Descripción
Backend para aplicación 'Oktli', que recibe peticiones por medio del servidor express, conectado a un cluster de MongoDB Atlas.
```
* Crea Usuario POST api/clients
* Listado de Usuarios GET api/clients
* Información de un Usuario GET api/clients/:email
* Crea movimiento POST api/movements
* Listado de Usuarios GET api/movements
* Movimientos de un Usuario GET api/movements/:email
```
## Instalación 
npm install

## Diagrama de Carpetas
```
|- conf/
|- controllers/
|- models/
|- index.js
|- package.json
|- package-lock.json
|- .gitignore
|- README.md
|- .dockerignore
|- Dockerfile
```

### Docker
* docker ps
* docker build -t backoktli .
* docker run -p 3001:3000 -d backoktli