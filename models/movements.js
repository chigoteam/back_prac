var mongoose = require('mongoose');
var Schema = mongoose.Schema;
//creo el esquema para los movimientos 
var movementSchema = new Schema({
    email: String,
    date: {
        type: Date,
        default: Date.now
    },
    type: {
        type: String,
        enum: ['Compra','Rechazo','Cancelacion','Actualizacion','Alta']
    },
    quantity: Number,
    product: String,
    cost: Number
},
{
    toObject: {
        virtuals: true, 
        getters: true
    },
    toJSON: {
        virtuals: true,
        getters: true
    }
}
);
//creo un campo virtual para que devuelva el total de la compra
movementSchema
.virtual('total')
.get(function(){
    return this.quantity*this.cost;
});
//devuelve la fecha en formato corto
movementSchema.virtual('date_s')
.get(function(){
    return this.date.toISOString().substring(0,10);
});

module.exports = mongoose.model('Movement',movementSchema);