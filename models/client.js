var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//creo el esquema para los clientes
var clientSchema = new Schema({
    firstName: { type: String },
    lastName: { type: String },
    email: { type: String, required: true, unique: true },
    password: { type : String, required:true },
    genre: { type: String, enum: ['male','female'] },
    address: {
        street: {type: String},
        number: {type: String},
        district: String, 
        cp: String,
        reference: String
    },
    location : {
        type: {
            type: String,
            enum: ['Point'],
            default: 'Point'
        }, 
        coordinates: {
            type: [Number]
        }
    }
});
module.exports = mongoose.model('Client', clientSchema);