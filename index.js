var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var methodOverride = require('method-override');
var configuration = require('./conf/configuration');
var app = express();

mongoose.connect(configuration.NEW_URI, configuration.BDCONF, function(err, res) {
    if (err) throw err;
    console.log('Connected to Database');
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(methodOverride());

var clientModel = require('./models/client')(mongoose);
var movementModel = require('./models/movements')(mongoose);
//console.log(models);
var ClientCtrl = require('./controllers/client');
var MovementCtrl = require('./controllers/movement');

var router = express.Router();

router.get('/', function(req, res){
    res.send("Server is listening to you!");
});
app.use(router);

var api = express.Router();
api.route('/movements')
    .get(MovementCtrl.findAll)
    .post(MovementCtrl.add);

api.route('/movements/:email')
    .get(MovementCtrl.findByMail);

api.route('/clients')
    .get(ClientCtrl.findAll)
    .post(ClientCtrl.add);

api.route('/clients/:email')
    .get(ClientCtrl.findByEmail)
    .put(ClientCtrl.update)
    .delete(ClientCtrl.delete);

app.use('/api',api);

app.listen(3000, function(){
    console.log("Node server running on http://localhost:3000");
});
